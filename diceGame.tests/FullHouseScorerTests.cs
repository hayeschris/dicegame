﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace diceGame.tests
{
    [TestClass]
    public class FullHouseScorerTests
    {
        [TestMethod]
        public void FullHouseReturns25()
        {
            var vals = new int[] { 1, 1, 1, 2, 2 };
            var scorer = new FullHouseScorer();
            var score = scorer.GetScore(vals);

            Assert.AreEqual(25, score);
        }

        [TestMethod]
        public void FullHouse2Returns25()
        {
            var vals = new int[] { 3, 1, 3, 1, 3 };
            var scorer = new FullHouseScorer();
            var score = scorer.GetScore(vals);

            Assert.AreEqual(25, score);
        }


        [TestMethod]
        public void NonFullHouseReturns0()
        {
            var vals = new int[] { 3, 1, 3, 1, 4 };
            var scorer = new FullHouseScorer();
            var score = scorer.GetScore(vals);

            Assert.AreEqual(0, score);
        }
    }
}
