﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace diceGame.tests
{
    [TestClass]
    public class SmallStraightScorerTest
    {
        [TestMethod]
        public void SmallStraightScores30()
        {
            var vals = new int[] { 2, 3, 4, 5, 7 };
            var scorer = new SmallStraightScorer();
            var score = scorer.GetScore(vals);

            Assert.AreEqual(30, score);
        }
        [TestMethod]
        public void SmallStraight2Scores30()
        {
            var vals = new int[] { 7, 4, 2, 3, 5 };
            var scorer = new SmallStraightScorer();
            var score = scorer.GetScore(vals);

            Assert.AreEqual(30, score);
        }
        [TestMethod]
        public void SmallStraight3Scores30()
        {
            var vals = new int[] { 5, 2, 3, 8, 4 };
            var scorer = new SmallStraightScorer();
            var score = scorer.GetScore(vals);

            Assert.AreEqual(30, score);
        }
        [TestMethod]
        public void SmallStraight4Scores30()
        {
            var vals = new int[] { 1, 4, 5, 6, 7 };
            var scorer = new SmallStraightScorer();
            var score = scorer.GetScore(vals);

            Assert.AreEqual(30, score);
        }
        [TestMethod]
        public void SmallStraight5Scores30()
        {
            var vals = new int[] { 4, 5, 6, 7, 1 };
            var scorer = new SmallStraightScorer();
            var score = scorer.GetScore(vals);

            Assert.AreEqual(30, score);
        }
        [TestMethod]
        public void SmallStraight6Scores30()
        {
            var vals = new int[] { 4, 5, 1, 7, 6 };
            var scorer = new SmallStraightScorer();
            var score = scorer.GetScore(vals);

            Assert.AreEqual(30, score);
        }
        [TestMethod]
        public void SmallStraight7Scores30()
        {
            var vals = new int[] { 4, 5, 8, 7, 6 };
            var scorer = new SmallStraightScorer();
            var score = scorer.GetScore(vals);

            Assert.AreEqual(30, score);
        }

        [TestMethod]
        public void SmallStraightAll1Scores30()
        {
            var vals = new int[] { 2, 3, 4, 5, 6 };
            var scorer = new SmallStraightScorer();
            var score = scorer.GetScore(vals);

            Assert.AreEqual(30, score);
        }
        [TestMethod]
        public void SmallStraightAll2Scores30()
        {
            var vals = new int[] { 6, 4, 3, 5, 2 };
            var scorer = new SmallStraightScorer();
            var score = scorer.GetScore(vals);

            Assert.AreEqual(30, score);
        }

        [TestMethod]
        public void NotSmallStraightScores0()
        {
            var vals = new int[] { 4, 5, 8, 5, 6 };
            var scorer = new SmallStraightScorer();
            var score = scorer.GetScore(vals);

            Assert.AreEqual(0, score);
        }
        [TestMethod]
        public void NotSmallStraight2Scores0()
        {
            var vals = new int[] { 5, 5, 8, 5, 6 };
            var scorer = new SmallStraightScorer();
            var score = scorer.GetScore(vals);

            Assert.AreEqual(0, score);
        }
        [TestMethod]
        public void NotSmallStraight3Scores0()
        {
            var vals = new int[] { 1, 2, 3, 6, 7 };
            var scorer = new SmallStraightScorer();
            var score = scorer.GetScore(vals);

            Assert.AreEqual(0, score);
        }
        [TestMethod]
        public void NotSmallStraight4Scores0()
        {
            var vals = new int[] { 6, 7, 3, 1, 2 };
            var scorer = new SmallStraightScorer();
            var score = scorer.GetScore(vals);

            Assert.AreEqual(0, score);
        }
    }
}
