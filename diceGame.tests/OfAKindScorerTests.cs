﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace diceGame.tests
{
    [TestClass]
    public class OfAKindScorerTests
    {
        [TestMethod]
        public void ThreeOfAKindScorerSumsCorrect()
        {
            var vals = new int[] { 1, 1, 1, 4, 5 };
            var scorer = new OfAKindScorer(3);
            var score = scorer.GetScore(vals);

            Assert.AreEqual(12, score);
        }
        [TestMethod]
        public void ThreeOfAKindScorerSumsCorrect2()
        {
            var vals = new int[] { 2, 1, 2, 4, 2 };
            var scorer = new OfAKindScorer(3);
            var score = scorer.GetScore(vals);

            Assert.AreEqual(11, score);
        }
        [TestMethod]
        public void ThreeOfAKindScorerSumsCorrect3()
        {
            var vals = new int[] { 3, 3, 4, 5, 3 };
            var scorer = new OfAKindScorer(3);
            var score = scorer.GetScore(vals);

            Assert.AreEqual(18, score);
        }
        [TestMethod]
        public void NotThreeOfAKindScorerSumsZero()
        {
            var vals = new int[] { 3, 2, 4, 5, 3 };
            var scorer = new OfAKindScorer(3);
            var score = scorer.GetScore(vals);

            Assert.AreEqual(0, score);
        }


        [TestMethod]
        public void FourOfAKindScorerSumsCorrect()
        {
            var vals = new int[] { 1, 1, 1, 4, 1 };
            var scorer = new OfAKindScorer(4);
            var score = scorer.GetScore(vals);

            Assert.AreEqual(8, score);
        }
        [TestMethod]
        public void FourOfAKindScorerSumsCorrect2()
        {
            var vals = new int[] { 2, 2, 2, 4, 2 };
            var scorer = new OfAKindScorer(4);
            var score = scorer.GetScore(vals);

            Assert.AreEqual(12, score);
        }
        [TestMethod]
        public void FourOfAKindScorerSumsCorrect3()
        {
            var vals = new int[] { 3, 3, 4, 3, 3 };
            var scorer = new OfAKindScorer(4);
            var score = scorer.GetScore(vals);

            Assert.AreEqual(16, score);
        }
        [TestMethod]
        public void NotFourOfAKindScorerSumsZero()
        {
            var vals = new int[] { 3, 2, 4, 5, 3 };
            var scorer = new OfAKindScorer(4);
            var score = scorer.GetScore(vals);

            Assert.AreEqual(0, score);
        }
    }
}
