﻿using System;
using System.Linq;
namespace diceGame
{
    public class AllOfAKindScorer : IScorer
    {
        public int GetScore(int[] dice)
        {
            if (dice.GroupBy(g => g).Count() == 1)
                return 50;
            return 0;
        }

        public string GetScorerName()
        {
            return $"all of a kind gets 50";
        }
    }
}
