﻿using System;
using System.Linq;
namespace diceGame
{
    public class ChanceScorer : IScorer
    {
        readonly bool _chance;
        public ChanceScorer()
        {
            _chance = new Random().Next(0, 1) == 1;
        }
        public int GetScore(int[] dice)
        {
            //if chance was meant to only apply to a 'chance' then use this code

            //if (_chance)
            //    return dice.Sum();
            //return 0;
            return dice.Sum();
        }

        public string GetScorerName()
        {
            return "chance score gets sum of all dice";
        }
    }
}
