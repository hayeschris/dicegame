﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace diceGame.tests
{
    [TestClass]
    public class LargeStraightScorerTests
    {
        [TestMethod]
        public void LargeStraightScorerReturns40()
        {
            var vals = new int[] { 1, 2, 3, 4, 5 };
            var scorer = new LargeStraightScorer();
            var score = scorer.GetScore(vals);

            Assert.AreEqual(40, score);
        }
        [TestMethod]
        public void LargeStraightScorer2Returns40()
        {
            var vals = new int[] { 5, 3, 4, 2, 1 };
            var scorer = new LargeStraightScorer();
            var score = scorer.GetScore(vals);

            Assert.AreEqual(40, score);
        }
        [TestMethod]
        public void LargeStraightScorer3Returns40()
        {
            var vals = new int[] { 8, 5, 6, 4, 7 };
            var scorer = new LargeStraightScorer();
            var score = scorer.GetScore(vals);

            Assert.AreEqual(40, score);
        }
        [TestMethod]
        public void NonLargeStraightScorerReturns0()
        {
            var vals = new int[] { 8, 5, 1, 4, 7 };
            var scorer = new LargeStraightScorer();
            var score = scorer.GetScore(vals);

            Assert.AreEqual(0, score);
        }
    }
}
