﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace diceGame.tests
{
    [TestClass]
    public class AllOfAKindScorerTests
    {
        [TestMethod]
        public void AllOfAKindEquals50(){
            var scorer = new AllOfAKindScorer();
            var vals = new int[] { 1,1,1,1,1};
            var score = scorer.GetScore(vals);
            Assert.AreEqual(50,score);
        }

        [TestMethod]
        public void NotAllOfAKindEquals0()
        {
            var scorer = new AllOfAKindScorer();
            var vals = new int[] { 1, 1, 1, 3, 1 };
            var score = scorer.GetScore(vals);
            Assert.AreEqual(0, score);
        }
    }
}
