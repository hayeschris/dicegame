﻿using System;
namespace diceGame
{

    public class DiceRoller : IDiceRoller
    {
        static Random rdm = new Random();
        readonly int _maxDiceSides;


        public DiceRoller(int maxDiceSides)
        {
            _maxDiceSides = maxDiceSides;
        }

        public int RollDice()
        {
            return rdm.Next(1, _maxDiceSides + 1);
        }
    }

}
