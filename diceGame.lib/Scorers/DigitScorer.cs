﻿using System;
using System.Linq;

namespace diceGame
{
    public class DigitScorer : IScorer
    {
        readonly int _digitValue;

        public DigitScorer(int digitValue)
        {
            _digitValue = digitValue;
        }

        public int GetScore(int[] dice)
        {
            var score = 0;
            dice.ToList().ForEach(d =>
            {
                if (d == _digitValue)
                {
                    score += d;
                }
            });

            return score;
        }

        public string GetScorerName()
        {
            return $"by Sum of all the {_digitValue}";
        }
    }

}
