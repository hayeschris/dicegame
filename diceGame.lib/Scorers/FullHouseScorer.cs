﻿using System;
using System.Linq;

namespace diceGame
{
    public class FullHouseScorer : IScorer
    {
        public int GetScore(int[] dice)
        {
            if (dice.GroupBy(g => g).Count() == 2 &&
               dice.GroupBy(g => g).Max(g => g.Count() == 3))
            {
                return 25;
            }
            return 0;
        }

        public string GetScorerName()
        {
            return "full house gets 25";
        }
    }
}
