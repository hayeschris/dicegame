﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace diceGame
{
    public static class Helpers
    {
        public static bool IsSequential(this IEnumerable<int> array)
        {
            return array.Zip(array.Skip(1), (a, b) => (a + 1) == b).All(x => x);
        }
    }
}
