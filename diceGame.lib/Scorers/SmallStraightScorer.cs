﻿using System;
using System.Linq;

namespace diceGame
{
    public class SmallStraightScorer : IScorer
    {
        public int GetScore(int[] dice)
        {
            var orderedDice = dice.Distinct().OrderBy(d => d);
            if (orderedDice.Count() < 4) return 0;
            if (orderedDice.Count() == 4)
            {
                if (orderedDice.IsSequential()) return 30;
            }
            else if (orderedDice.Take(4).IsSequential() || orderedDice.Skip(1).IsSequential())
                return 30;
            return 0;
        }

        public string GetScorerName()
        {
            return "small straight gets 30";
        }
    }
}
