﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace diceGame.tests
{
    [TestClass]
    public class ChanceScorerTests
    {
        [TestMethod]
        public void ChanceScorerSumsAllVals(){
            var vals = new int[] { 1,2,3,4,5};
            var scorer = new ChanceScorer();
            var score = scorer.GetScore(vals);

            Assert.AreEqual(15, score);
        }

        [TestMethod]
        public void ChanceScorerSumsAllVals2()
        {
            var vals = new int[] { 5, 2, 8, 4, 8 };
            var scorer = new ChanceScorer();
            var score = scorer.GetScore(vals);

            Assert.AreEqual(27, score);
        }
    }
}
