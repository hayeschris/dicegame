﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace diceGame.tests
{
    [TestClass]
    public class NoneOfAKindScorerTests
    {
        [TestMethod]
        public void NoneOfAKindReturns40()
        {
            var vals = new int[] { 1, 2, 3, 4, 5 };
            var scorer = new NoneOfAKindScorer();
            var score = scorer.GetScore(vals);
            Assert.AreEqual(40, score);
        }
        [TestMethod]
        public void NoneOfAKind2Returns40()
        {
            var vals = new int[] { 8, 2, 3, 7, 5 };
            var scorer = new NoneOfAKindScorer();
            var score = scorer.GetScore(vals);
            Assert.AreEqual(40, score);
        }
        [TestMethod]
        public void NoNoneOfAKindReturns0()
        {
            var vals = new int[] { 8, 2, 3, 5, 5 };
            var scorer = new NoneOfAKindScorer();
            var score = scorer.GetScore(vals);
            Assert.AreEqual(0, score);
        }
    }
}
