﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace diceGame
{
    class Program
    {
        private static string _help;
        static void Main(string[] args)
        {
            var game = new DiceGame(new DiceRoller(8));
            BuildHelp();
            Console.WriteLine("Welcome to the dice game!");
            Instructions();
            ConsoleKey keyval;
            while ((keyval = Console.ReadKey(true).Key) != ConsoleKey.Escape)
            {
                if (keyval == ConsoleKey.H)
                {
                    Console.WriteLine(_help);
                }
                else
                {
                    game.Roll();
                    Console.WriteLine("You rolled:");
                    Console.WriteLine(game.ShowDice());
                    Console.WriteLine(game.ShowResult());
                }
                Instructions();
            }
        }

        private static void Instructions()
        {
            Console.WriteLine("Press any key to continue rolling dice");
            Console.WriteLine("Press <ESC> to quit");
            Console.WriteLine("Press 'H' for help");
            Console.WriteLine(new string('=',10));
        }

        private static void BuildHelp()
        {
            var bldr = new StringBuilder();
            bldr.Append("You have five eight-sided dice, and based on your roll, you can get a certain score as outlined below. Given a five-item array of integers (which represent the values rolled), return the highest possible score.\r\n");
            bldr.Append("\r\n");
            bldr.Append("Ones – scores the sum of all the ones\r\n");
            bldr.Append("Twos – scores the sum of all the twos\r\n");
            bldr.Append("Threes – scores the sum of all the threes\r\n");
            bldr.Append("Fours – scores the sum of all the fours\r\n");
            bldr.Append("Fives – scores the sum of all the fives\r\n");
            bldr.Append("Sixes – scores the sum of all the sixes\r\n");
            bldr.Append("Sevens – scores the sum of all the sevens\r\n");
            bldr.Append("Eights – scores the sum of all the eights\r\n");
            bldr.Append("ThreeOfAKind – scores the sum of all dice if there are 3 or more of the same die, otherwise scores 0\r\n");
            bldr.Append("FourOfAKind – scores the sum of all dice if there are 4 or more of the same die, otherwise scores 0\r\n");
            bldr.Append("AllOfAKind – Scores 50 if all of the dice are the same, otherwise scores 0\r\n");
            bldr.Append("NoneOfAKind – Scores 40 if there are no duplicate dice, otherwise scores 0\r\n");
            bldr.Append("FullHouse – Scores 25 if there are two duplicate dice of one value and three duplicate dice of a different value, otherwise scores 0\r\n");
            bldr.Append("SmallStraight – Scores 30 if there are 4 or more dice in a sequence, otherwise scores 0\r\n");
            bldr.Append("LargeStraight – Scores 40 if all 5 dice are in a sequence, otherwise scores 0\r\n");
            bldr.Append("Chance – scores the sum of all dice\r\n");
            bldr.Append("\r\n");
            bldr.Append("For example, if you roll the following: 1, 1, 1, 4, 8\r\n");
            bldr.Append("\r\n");
            bldr.Append("This roll will match the following rules and corresponding scores:\r\n");
            bldr.Append("Ones – score of 3\r\n");
            bldr.Append("Fours – score of 4\r\n");
            bldr.Append("Eights – score of 8\r\n");
            bldr.Append("Chance – score of 15\r\n");
            bldr.Append("ThreeOfAKind – score of 15\r\n");
            bldr.Append("\r\n");
            bldr.Append("So you would return 15 since that’s the highest possible score.\r\n");
            _help = bldr.ToString();
        }
    }
}
