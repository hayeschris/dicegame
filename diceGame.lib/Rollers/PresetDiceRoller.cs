﻿using System;
namespace diceGame
{


    public class PresetDiceRoller : IDiceRoller
    {
        int _location = 0;
        int[] _presets;
        public PresetDiceRoller(int[] presets)
        {
            _presets = presets;
        }
        public int RollDice()
        {
            var output = _presets[_location];
            _location++;
            if (_location >= _presets.Length) _location = 0;
            return output;
        }
    }

}
