﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace diceGame.tests
{
    [TestClass]
    public class PresetRollerTests
    {
        [TestMethod]
        public void PresetRollerReturnsValsGiven()
        {
            var dice = new int[] { 1, 2, 3, 4, 5 };
            var roller = new PresetDiceRoller(dice);
            var rolled = Enumerable.Range(0, 4)
                                   .Select(i => roller.RollDice());
            var allmached = dice.Zip(rolled,
                (given, val) => given == val).All(m => m);
            Assert.IsTrue(allmached);
        }
        [TestMethod]
        public void PresetRollerReturnsValsGiven2()
        {
            var dice = new int[] { 4, 1, 5, 7, 8 };
            var roller = new PresetDiceRoller(dice);
            var rolled = Enumerable.Range(0, 4)
                                   .Select(i => roller.RollDice());
            var allmached = dice.Zip(rolled,
                (given, val) => given == val).All(m => m);
            Assert.IsTrue(allmached);
        }
    }
}
