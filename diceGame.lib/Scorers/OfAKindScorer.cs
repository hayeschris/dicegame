﻿using System;
using System.Linq;

namespace diceGame
{
    public class OfAKindScorer : IScorer
    {
        readonly int _numberOfAKind;
        public OfAKindScorer(int numberOfAKind)
        {
            _numberOfAKind = numberOfAKind;
        }
        public int GetScore(int[] dice)
        {
            var grps = dice.GroupBy(d => d);
            if (grps.Any(g => g.Count() == _numberOfAKind))
            {
                return dice.Sum();
            }
            return 0;
        }

        public string GetScorerName()
        {
            return $"by Sum of {_numberOfAKind} of a kind";
        }
    }
}
