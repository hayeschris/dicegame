﻿using System;
using System.Linq;

namespace diceGame
{
    public class NoneOfAKindScorer : IScorer
    {
        public int GetScore(int[] dice)
        {
            if (dice.GroupBy(g => g).Count() == 5)
                return 40;
            return 0;
        }

        public string GetScorerName()
        {
            return $"none of a kind gets 40";
        }
    }
}
