﻿using System;
namespace diceGame
{

    public interface IScorer
    {
        int GetScore(int[] dice);
        string GetScorerName();
    }

}
