﻿using System;
namespace diceGame
{

    public interface IDiceRoller
    {
        int RollDice();
    }
}
