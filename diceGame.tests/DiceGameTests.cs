﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace diceGame.tests
{
    [TestClass]
    public class DiceGameTests
    {
        [TestMethod]
        public void DiceGameTests_example()
        {
            var game = new DiceGame(new PresetDiceRoller(new[] { 1, 1, 1, 4, 8 }));

            var score = game.MaxScore();
            //example in specs
            Assert.AreEqual(15, score);
            Assert.IsTrue(game.MaxScorer is ChanceScorer || game.MaxScorer is OfAKindScorer);
        }
        [TestMethod]
        public void DiceGameTests_all_of_a_kind_one_50()
        {
            var game = new DiceGame(new PresetDiceRoller(new[] { 1, 1, 1, 1, 1 }));

            var score = game.MaxScore();
            //all of a kind 50
            Assert.AreEqual(50, score);
            Assert.IsTrue(game.MaxScorer is AllOfAKindScorer);
        }
        [TestMethod]
        public void DiceGameTests_four_of_a_kind_sum_9()
        {
            var game = new DiceGame(new PresetDiceRoller(new[] { 1, 1, 1, 1, 5 }));

            var score = game.MaxScore();
            //four of a kind sums all
            Assert.AreEqual(9, score);
            Assert.IsTrue(game.MaxScorer is OfAKindScorer);
        }
        [TestMethod]
        public void DiceGameTest_three_of_a_kind_sum_12()
        {
            var game = new DiceGame(new PresetDiceRoller(new[] { 1, 1, 1, 4, 5 }));

            var score = game.MaxScore();
            //three of a kind sums all
            Assert.AreEqual(12, score);
            Assert.IsTrue(game.MaxScorer is OfAKindScorer);
        }
        [TestMethod]
        public void DiceGameTest_full_house_25()
        {
            var game = new DiceGame(new PresetDiceRoller(new[] { 1, 1, 1, 4, 4 }));

            var score = game.MaxScore();
            //full house 25
            Assert.AreEqual(25, score);
            Assert.IsTrue(game.MaxScorer is FullHouseScorer);
        }
        [TestMethod]
        public void DiceGameTest_all_of_a_kind_50()
        {
            var game = new DiceGame(new PresetDiceRoller(new[] { 8, 8, 8, 8, 8 }));

            var score = game.MaxScore();
            //all of a kind 50
            Assert.AreEqual(50, score);
            Assert.IsTrue(game.MaxScorer is AllOfAKindScorer);
        }
        [TestMethod]
        public void DiceGameTest_none_of_a_kind_40()
        {
            var game = new DiceGame(new PresetDiceRoller(new[] { 4, 8, 2, 5, 6 }));

            var score = game.MaxScore();
            //none of a kind 40
            Assert.AreEqual(40, score);
            Assert.IsTrue(game.MaxScorer is NoneOfAKindScorer);
        }
        [TestMethod]
        public void DiceGameTest_small_straight_30()
        {
            var game = new DiceGame(new PresetDiceRoller(new[] { 2, 3, 4, 5, 3 }));

            var score = game.MaxScore();
            //small straight is 30
            Assert.AreEqual(30, score);
            Assert.IsTrue(game.MaxScorer is SmallStraightScorer);
        }
        [TestMethod]
        public void DiceGameTest_large_straight_40()
        {
            var game = new DiceGame(new PresetDiceRoller(new[] { 2, 6, 4, 3, 5 }));

            var score = game.MaxScore();
            //large straight is 40
            Assert.AreEqual(40, score);
            Assert.IsTrue(game.MaxScorer is LargeStraightScorer || game.MaxScorer is NoneOfAKindScorer);
        }
        [TestMethod]
        public void DiceGameTest_chance()
        {
            var game = new DiceGame(new PresetDiceRoller(new[] { 2, 7, 4, 2, 8 }));

            var score = game.MaxScore();
            //chance is sum
            Assert.AreEqual(23, score);
            Assert.IsTrue(game.MaxScorer is ChanceScorer);
        }
    }
}
