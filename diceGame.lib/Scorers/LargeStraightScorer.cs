﻿
using System;
using System.Linq;

namespace diceGame
{
    public class LargeStraightScorer : IScorer
    {
        public int GetScore(int[] dice)
        {
            if (dice.OrderBy(d => d).IsSequential())
                return 40;
            return 0;
        }

        public string GetScorerName()
        {
            return "large straight gets 40";
        }
    }
}
