﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace diceGame
{
    public class DiceGame
    {
        readonly IDiceRoller _roller;
        List<Func<IScorer>> _scorers = new List<Func<IScorer>>
        { // these are in the order of the spec
          // if you don't move the 'NoneOfAKindScorer' below
          // 'LargeStraightScorer' then 'LargeStraightScorer'
          // will never be seleced
            ()=>new DigitScorer(1),
            ()=>new DigitScorer(2),
            ()=>new DigitScorer(3),
            ()=>new DigitScorer(4),
            ()=>new DigitScorer(5),
            ()=>new DigitScorer(6),
            ()=>new DigitScorer(7),
            ()=>new DigitScorer(8),
            ()=>new OfAKindScorer(3),
            ()=>new OfAKindScorer(4),
            ()=>new AllOfAKindScorer(),
            ()=>new NoneOfAKindScorer(),
            ()=>new FullHouseScorer(),
            ()=>new SmallStraightScorer(),
            ()=>new LargeStraightScorer(),
            ()=>new ChanceScorer()
        };
        int[] _rolledDice;
        IScorer _maxScorer;

        public IScorer MaxScorer => _maxScorer;

        public string ShowDice()
        {
            var dice = _rolledDice.ToArray();
            return $"[{dice[0]}, {dice[1]}, {dice[2]}, {dice[3]}, {dice[4]}]";
        }

        public string ShowResult()
        {
            return $"Your max possible score was {_maxScorer.GetScore(_rolledDice)} {_maxScorer.GetScorerName()}";

        }

        public int MaxScore()
        {
            Roll();
            return GetScoreAfterRoll();
        }

        public int MaxScoreIfRolled()
        {
            if (_maxScorer == null)
                throw new ApplicationException("you can not run this method without rolling first");

            return GetScoreAfterRoll();
        }

        int GetScoreAfterRoll()
        {
            return _maxScorer.GetScore(_rolledDice);
        }

        public DiceGame(IDiceRoller roller)
        {
            _roller = roller;
        }

        public void Roll()
        {
            _rolledDice = Enumerable.Range(0, 5).Select(i => _roller.RollDice()).ToArray();

            _maxScorer = _scorers.Select(x => x()).OrderByDescending(s => s.GetScore(_rolledDice)).First();

        }
        
    }
}
