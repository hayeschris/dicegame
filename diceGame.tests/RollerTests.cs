using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace diceGame.tests
{
    [TestClass]
    public class RollerTests
    {
        [TestMethod]
        public void RollerReturnsMaxUnder()
        {
            var eight = 8;
            var roller = new DiceRoller(eight);

            var anyOverEight = Enumerable.Range(0, 10000)
                                         .Any(x => roller.RollDice() > eight);

            Assert.IsFalse(anyOverEight);
        }

        [TestMethod]
        public void RollerReturnsMinAbove1()
        {
            var eight = 8;
            var roller = new DiceRoller(eight);

            var anyLessThanOne = Enumerable.Range(0, 10000)
                                         .Any(x => roller.RollDice() < 1);

            Assert.IsFalse(anyLessThanOne);
        }
    }
}
