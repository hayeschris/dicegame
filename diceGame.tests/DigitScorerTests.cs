﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
namespace diceGame.tests
{
    [TestClass]
    public class DigitScorerTests
    {
        [TestMethod]
        public void DigitScorerOnesMatchVal()
        {
            var vals = new int[] { 1, 2, 1, 1, 3 };
            var scorer = new DigitScorer(1);
            var score = scorer.GetScore(vals);

            Assert.AreEqual(3, score);
        }
        [TestMethod]
        public void NotDigitScorerOnesMatchVal()
        {
            var vals = new int[] { 4, 2, 4, 4, 3 };
            var scorer = new DigitScorer(1);
            var score = scorer.GetScore(vals);

            Assert.AreEqual(0, score);
        }

        [TestMethod]
        public void DigitScorerTwosMatchVal()
        {
            var vals = new int[] { 1, 2, 1, 2, 3 };
            var scorer = new DigitScorer(2);
            var score = scorer.GetScore(vals);

            Assert.AreEqual(4, score);
        }
        [TestMethod]
        public void NotDigitScorerTwosMatchVal()
        {
            var vals = new int[] { 4, 5, 4, 4, 3 };
            var scorer = new DigitScorer(2);
            var score = scorer.GetScore(vals);

            Assert.AreEqual(0, score);
        }

        [TestMethod]
        public void DigitScorerThreesMatchVal()
        {
            var vals = new int[] { 3, 2, 1, 2, 3 };
            var scorer = new DigitScorer(3);
            var score = scorer.GetScore(vals);

            Assert.AreEqual(6, score);
        }
        [TestMethod]
        public void NotDigitScorerThreesMatchVal()
        {
            var vals = new int[] { 4, 5, 4, 4, 8 };
            var scorer = new DigitScorer(3);
            var score = scorer.GetScore(vals);

            Assert.AreEqual(0, score);
        }


        [TestMethod]
        public void DigitScorerFivesNoMatchVal()
        {
            var vals = new int[] { 1, 2, 1, 2, 3 };
            var scorer = new DigitScorer(5);
            var score = scorer.GetScore(vals);

            Assert.AreEqual(0, score);
        }
    }
}
